// KeyState.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "KeyState.h"
#include <stdio.h>

#define MAX_LOADSTRING 100

HINSTANCE hInst;
TCHAR szTitle[MAX_LOADSTRING];
TCHAR szWindowClass[MAX_LOADSTRING];

/*******************************************************************************
Global vars
*///////////////////////////////////////////////////////////////////////////////
SHORT sync, async;  // retrun values from GetKeyState() and GetAsyncKeyState()
INPUT ctrlup;       // INPUT struc to send on each WM_KEYDOWN of the control key
RECT redraw;        // redraw region for painting
BOOL isDown;        // to track 'real' key state

LPCTSTR explanation = _T("GetKeyState() returns value reflecting the last READ key up event message (it is still down).");
LPCTSTR explanationa = _T("GetAsyncKeyState() returns value reflecting POSTED key up event message (it is up - because of the SendInput()).");
////////////////////////////////////////////////////////////////////////////////

ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPTSTR    lpCmdLine,
    int       nCmdShow)
{
    MSG msg;

    /***************************************************************************
    Setup global vars
    *///////////////////////////////////////////////////////////////////////////
    ctrlup.type = INPUT_KEYBOARD;
    ctrlup.ki.wVk = VK_CONTROL;
    ctrlup.ki.dwFlags = KEYEVENTF_KEYUP;

    ctrlup.ki.dwExtraInfo = 1; // to enable tracking of 'real' key state

    SetRect(&redraw, 0, 0, 300, 300);
    ////////////////////////////////////////////////////////////////////////////

    LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadString(hInstance, IDC_KEYSTATE, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }
    
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int) msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style			= CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc	= WndProc;
    wcex.cbClsExtra		= 0;
    wcex.cbWndExtra		= 0;
    wcex.hInstance		= hInstance;
    wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_KEYSTATE));
    wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_KEYSTATE);
    wcex.lpszClassName	= szWindowClass;
    wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassEx(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
    HWND hWnd;

    hInst = hInstance;

    hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

    if (!hWnd)
    {
        return FALSE;
    }

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    int wmId, wmEvent;
    PAINTSTRUCT ps;
    HDC hdc;

    TCHAR buffer[32]; // for formatting output

    switch (message)
    {
    case WM_KEYDOWN:
        /***********************************************************************
        Send control up event as soon as control is pressed and update global
        status vars - async SHOULD still report that cntl is down
        *///////////////////////////////////////////////////////////////////////
        if (wParam == VK_CONTROL)
        {
            SendInput(1, &ctrlup, sizeof(INPUT));

            sync = HIWORD(GetKeyState(VK_CONTROL));
            async = HIWORD(GetAsyncKeyState(VK_CONTROL));

            isDown = 1;
            InvalidateRect(hWnd, &redraw, 1);
            UpdateWindow(hWnd);
        }
        ////////////////////////////////////////////////////////////////////////

        break;
    case WM_KEYUP:
        /***********************************************************************
        Track 'real' key state and update values
        *///////////////////////////////////////////////////////////////////////
        if (GetMessageExtraInfo() != 1) // not the simulated key up from SendInput()
        {
            sync = HIWORD(GetKeyState(VK_CONTROL));
            async = HIWORD(GetAsyncKeyState(VK_CONTROL));

            isDown = 0;
            InvalidateRect(hWnd, &redraw, 1);
            UpdateWindow(hWnd); 
        }
        ////////////////////////////////////////////////////////////////////////
        break;
    case WM_COMMAND:
        wmId    = LOWORD(wParam);
        wmEvent = HIWORD(wParam);

        switch (wmId)
        {
        case IDM_EXIT:
            DestroyWindow(hWnd);
            break;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
        break;
    case WM_PAINT:
        hdc = BeginPaint(hWnd, &ps);

        /***************************************************************************
        Output results
        *///////////////////////////////////////////////////////////////////////////
        TextOut(hdc, 0, 0, _T("Press and Release Left CNTL..."), 30);

        _stprintf(buffer, _T("GetKeyState: %x"), sync);
        TextOut(hdc, 0, 50, buffer, _tcslen(buffer));

        _stprintf(buffer, _T("GetAsyncKeyState: %x"), async);
        TextOut(hdc, 0, 100, buffer, _tcslen(buffer));

        _stprintf(buffer, _T("'Real' key state: %s"), isDown ? _T("Down") : _T("Up"));
        TextOut(hdc, 0, 150, buffer, _tcslen(buffer));
        
        TextOut(hdc, 0, 200, _T("Explanation:"), 12);
        TextOut(hdc, 0, 220, explanation, _tcslen(explanation));
        TextOut(hdc, 0, 240, explanationa, _tcslen(explanationa));
        ////////////////////////////////////////////////////////////////////////////

        EndPaint(hWnd, &ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}
